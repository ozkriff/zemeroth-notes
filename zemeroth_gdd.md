
# Zemeroth

Player's avatar wants to become a demon lord. (__TODO__: why?)

"Zemeroth" is his demon-name.

To achieve this, player should conceal his intensions and control
humans so they won't understand what's going on until the player
is too powerful.

Player may corrupt some humans with demonic powers.
During the course of the game he can give "demon blessings" to his followers.

Player should communicate secretly with demons, perform dark rites,
learn dark spells (?) and collect artifacts.

The player has some demonic abilities (both tactical and strategic),
but he can't use them publicly without vexing his human masters.

__TODO__: What's the overall player's motivation?
Is he just greedy for the power?
Or for an immortality?
Maybe he was deceived by demons kinda like Horus?
Is this a revenge like in "Am I evil?" song.

## Game Identity

"Minimalistic turn-based strategy about a cultist in disguise"

__TODO__: try to improve.

## Game Pillars

TODO: !!!

- output randomness is ok, the player shouldn't control everything
- minimalistic (kinda like Into the Breach)
- the player is evil but has some "good" parts (__TODO__: rephrase)

## Key Features

__TODO:__ Hmm...

- Dynamic tactical battles (TODO: describe what dynamic means in this context)
- Open-Source :-D
- Deceive your enemies, betray your "friends"
- ???

## Platforms

At the moment Win/Linux/macOS are supported.

__NOTE__: Waiting for `ggez` v0.5 with `winit` to get back Android.

Web (emscripten or WASM) is planned, but no work has been done
in this direction yet.

__TODO__: How will the save system will work with web version???

__TODO__: At what platforms/stores am I going to publish the game?

Itch? Google Play?

## Masters & quests

__TODO__: rename "masters" to something.

__TODO__: split and/or move this section

At each moment of time the player has at least one master
who gives the player quests (tasks).

Every master has a "loyalty" (rename?) parameter that shows
how loyal the player looks to them.
The bigger "loyalty", the more values the master gives to the player.

The player isn't forced to publicly declare all his masters.
A scenario when the player publicly servers to a royal human master,
but also secretly server to a demon lord is a port of the plot.

__TODO__: The player should have a way to control his masters or alter
their opinions  to a some degree.

The main player's task
is to switch demon masters to a more powerful ones,
util he slowly becomes a demon himself,
and at the same time player should weaken/subjugate human masters.

Eventually, the player receives the ability to gift mutations
to his followers (and himself?), making them partly demonic and more powerful.
But such followers should be hidden from normal (__TODO__: reword) humans.

Satisfied demonic masters can give some demons followers to the player.
These followers should be hidden from the human world even better.
Hide them in the forests until they are needed for the operation.

__TODO__: How mutations work?
Are they like items/artifacts that can't be removed?

__TODO__: How these mutations 9and real demon followers) can be hidden?

__TODO__ how these mutations should be visualized?
Combined sprite?
Or the follower just changes his class to "mutated folover"?

Human masters have a "tolerance" parameter that shows how much magic/demonic
tricks can they tolerate while you are getting their tasks done.

Quests form different masters should compete for player's attention,
forcing him to make (i hope) difficult choices.

Some masters might be just blackmailers, forcing you to do some work for them.

## Inspiration

__TODO__: describe what ideas is used from which project.

The tactical layer is inspired by:

- [Banner Saga](https://store.steampowered.com/app/237990/The_Banner_Saga)
- [Hoplite](https://play.google.com/store/apps/details?id=com.magmafortress.hoplite)
- [Auro](https://store.steampowered.com/app/459680/Auro_A_MonsterBumping_Adventure)
- [Age of Decadence](https://store.steampowered.com/app/230070/The_Age_of_Decadence)
- [Into the Breach](https://store.steampowered.com/app/590380/Into_the_Breach)

The strategy layer is inspired by:

- [Cultist Simulator](https://store.steampowered.com/app/718670/Cultist_Simulator)
- [This is the Police](https://store.steampowered.com/app/443810/This_Is_the_Police)

Others:

- Battle Brothers
- Pit People
- Urtuk: the Desolation
- Outwitters
- Guards of the Gate (TODO: investigate)
- Wartile (TODO: investigate)
- ShardBound (???)
- Battlelore command (???)

## Artistic Style Outline

"2D pseudo-lowpoly".
Try not to use any round shapes, textures or gradients.

__Q:__ how will be the demonic mutations visualized?
__A:__ I guess it's better to use different unit types and
thus completely different sprites.
It should align well with mutated followers getting other abilities.

## Overall Plot

- fight raiders/theifs
- fight small demons
- establish a contact with demons
- get a demon "master" and a promise of power for your help. keep it in a secret
- collect amulets / kill monks or guards
- someone tries to blackmail you
- fight more demons (because human master says so)
- do the rite and summon demon invasion
- make human armies weaker by sabotaging them from the inside
- reveal yourself as a demon follower
- help demons fight human lords
- become a deamon yourself
- become a daemon-lord of human lands and fight with competing demons

__TODO:__ It would be cool to allow the player to hide something _from_
demons after the mid game.

## Quest/Events

__TODO__: __Q:__: What happens when event's time is up?

### Quest/Events Examples

__TODO__: ???

## Save/Load

Permadeath-only.

TODO: ???

## __H_O_R_S_E_S__ (__TODO__: ???)

__TODO:__ __Q:__ Does this world has horses?
Are they used in battles (tactical and strategic?)
Are they used in strategic mode?
Logistics?

One though is that most of human animals can be dead for some reason.
Horse plague? )

## Global/Strategy map

![a crude draft of the global map](https://i.imgur.com/w6VYrs2.png)

Castle gives a big defence bonus to a strategic battles.
Though, other types of terrain can be useful in battles too.

Tile types:

- Forest (allows to hide? __TODO__: how does it work?)
- Village
- City
- Castle
- Demonic portal
- __TODO__: more

Player's main squad and strategic troops should be able to move multiple
tiles per turn. The exact distance depends on their type, MPs and the terrain.

__TODO__: describe how quest/event markers work

There's no real moving enemy armies on the global map.
This should be emulated with half-scripted event markers.

__TODO__: __Q__: Are enemies cities/castles/villages/mines really _work_?
Is there a real complex simulation behind their actions?
__A__: I think that it's better to keep this thing simple
and don't actually use complex whole-world simulations.
Just half-random/half-scripted quest/event marks.

__TODO__: __Q:__ Are there any strategic troops of the enemy
moving around the global map?
__A:__ Nope, only player has the strategic troops.
(think more about this)

__TODO:__ __Q:__ Can we capture the slaves?
Can we use them for anything (like resource gathering in mines)?

### Cities, Castles, etc

TODO: How do these places look "inside"?
Text-only menus or some visual "Banner Saga"-like clickable representation?

Not sure, but I guess it's better to this with text only version for now.
It's just a visual thing, right? So it can be upgrades
to a visual representation later.

__TODO__: Can castles/cities/villages/etc be damaged during
strategic or tactical battles?

### Strategic battles

__TODO:__ Is it possible to escape from the strategic or tactical battle?

__TODO__: what kinds of strategic troops exist?

__TODO__: the player should be able to combine different types of global troops
to get some bonuses

__TODO:__ think of "Battle of the Bulge"-style of battles:

![picture from Battle of the Bulge](https://i.imgur.com/pp5GIyem.png)

__TODO:__ how exactly do player's avatar, followers and strategic troops
work together?

If the player has his avatar in the battle tile he can start a tactical battle.
If there's not troops (only the avatar and followers) there will be no
strategic battle after that.
If the avatar is leading the troops, then a result of a tactical battle
defines what bonus will your troop get for their strategic battle.

__TODO__: are tactical battles only allowed before the strategic one?
Maybe a strategic battle can be split into the "episodes" and
tactical battles can happen between them with each type of battle
influencing the other one?

__TODO__: a very important question - how castles are stormed/defended?
Tactical battles can't really show the walls and other stuff.

__TODO__: __Q:__: Why can't the player just ignore tactical battles
for the whole game?
__A:__ in the early game the play simply doesn't have global troops so
they is forced to participate in tactical battles.
it the mid or end game stages the player's strategic troops should
be outnumbered so they will have no real chance of winning
without bonuses from tactical battles.

__TODO__: Troops' fatigue?

__TODO__: recovering from wounds is an important concept in xcom-like games
that requires player to rotate his team member so they all get the experience.
How should I do something like this in Zemeroth?
What should we do with player's avatar
as he's always required for tactical battles?

### Global timer

There should be a global (strategic) timer to force player take risky decisions.
Like that all masters loose "loyalty" (TODO) over the time
forcing the player to act quicker.

__TODO__: Maybe some scripted quests could be used for this? Think this out.

### Global economy

__TODO__: what kind or resources does the game need? Is money enough?

__TODO__: What's the economy of demonic game phase?
Money/Gold doesn't make much sense there. Souls? Slaves? Reputation?

The center of the game is the tactical battles.
The most valuable resource for them is your followers - their count
and quality (types, experience, etc).
__TODO__: strategic troops?

## Tactical battles

TODO: copy most of the old russian "proto-GDD" here

Maps are always have 11 tiles in radius.

All types of missions should have some kind of timer too (like the global map).

Battle types:

- Escort some VIP to an exit tile (protect him for N turns)
- Destroy some on-map object (break the sphere to close the portal?)
- __TODO__: more

## Dialogs

![a crude illustration of dialog screen](https://i.imgur.com/Q0MHOL5.png)

__TODO__: ?

Some dialogs may happen during the battle:
for example, you kill the gang's leader - others may try to flee
or ask if they can join you.

The dialog system uses a giant `HashMap<String, String>` that is a part of
the global game state and can be reached from both tactical and global screens.

__TODO__: How conditions are determined? Do I need some `enum` for this?
__TODO__: How can conditions access to the game's state

__TODO__: Do I need text quests? I'd like to avoid lots of text.

## Game Flow Summary

__TODO__: __!!!!!!!!!!!!!__ that's an important section

Villages, cities, mines etc gives the player money that can be used:

- in quests (pay for an assassination, buy someone's silence, bribe someone)
- to improve your followers
- to pay for strategic troops

Demons are not interested in human's money.
They only value the reputation.

__TODO__: How exactly the demonic reputation is gained?
Quests? Victories in battles?

## Resurrection

After a few battles the player should sign a deal with demons.
They are not interested in him dying.
They give the player an ability to resurrect after a death in his hiding place.
This works while the place isn't destroyed and the player
has required resources for the resurrection.
(__TODO__ what resources? sacrafices? life for a life?).

__TODO__: Should the player be able to participate in the battles themselves?
From one point of view, an xcom-like meta structure means that any
agent in your squad may die instantly, but the whole strategy game won't end
on that - you'll just create another team and send it on the next mission.
This obviously doesn't work if the player has a direct avatar on
the battlefield.

Yeah, a player's avatar is always required for tactical battle.
Though, in early stages of the game demons give you the ability to resurrect.
If you die in a battle, you soon reborn in your hiding place (?).

__TODO:__ who will hunt for this secret place? Examples of quests/events?

- The secret place is attacked

__TODO:__ Can player move their place when it's discovered?
If yes, how much does it cost?

## Game Screens

- Main Menu
  - background image
  - buttons:
    - start campaign
    - start battle (__TODO__: what exactly should it do?)
    - options (__TODO__)
    - quit
- Global
- Battle
- Dialog
- Squad (?)

## Agents

__TODO__: is there a point in splitting agents into "Human agent types"
and "Demonic agent types"?

__TODO__: List agent types or some categories. What stats do they have?

### Characters

__TODO__: Do we need special subset of followers who will have a portrait,
their own plot arc with dialogs and quests and unique abilities?

## Artefact (Item) System

Simple slot system like in
[BS](https://bannersaga.gamepedia.com/The_Banner_Saga_items).

Item examples:

- Some ring that gives you move points but reduces your strength
- __TODO__: ?

__TODO__: Think through item system interface!

## Kidnaping, Torment, Sacrifice

Kidnap monks, torture them to find where the sanctuaries are,
sacrifice them to the dark gods.

(Think of xcom-like investigation or something)

Sacrifice your own followers if you really need some "demon points"
or if they are useless.

## Audio

__TODO__: link to "slow drums" issue. Copy some description from there.

__TODO__: interface sounds?
__TODO__: UI sounds? clicks?
__TODO__: battle sounds? weapons, screams

__Q__: speech? do i need it? __A__: __TODO__?

## FAQ

- __Q:__ Why assets are stored in a separate repo with manual sync?

  __A:__ (__TODO__ - submodules, hash file, users should use releases,
  developer-only)

- __Q:__ Why singleplayer only?

  __A:__ (__TODO__ - too complicated, just want to finish the game)

- __TODO__: More questions?

## Roadmap

- [ ] Basic Strategic Screen:
  - [ ] Castle that gives you gold
  - [ ] gui to show your gold
  - [ ] Moving squad
    - [ ] permanent list of followers (used for tactical battle)
    - [ ] army
  - [ ] Ability to buy follower in town for gold
  - [ ] Few pregenerated event markers
    - [ ] a tactical battle, then a strategic battle (with tactical bonus)
    - [ ] One event should have a super-simple demo dialog with three options:
      - start a strategic battle
      - start a tactical battle, then strategic one but with a bonus.
        Bonus depends on the result of the tactical battle.
      - quit the event (with the ability to start it again)
- GlobalMap Screen (TODO: what exactly? split into more tasks)
  - cities
  - forests
  - quest/event markers
  - player agent
  - __TODO__: Do I need other agents?
- Basic Quest system
- Basic NPC/Agent/Masters system (TODO: more details)
- Basic Dialogue system
  - __TODO__: how should this be implemented?
    - __TODO__: how to store the lines
    - __TODO__: how to store the results?
    - __TODO__: events?
- Move to dodge (more movement) (<https://github.com/ozkriff/zemeroth/issues/117>)
- Weight component (<https://github.com/ozkriff/zemeroth/issues/291>)
- Deployment phase for tactical battles (TODO: create an issue)
- Unit upgrades (TODO: create an issue)
- Squad Screen (TODO: more details)
- Dynamic blood splatters (<https://github.com/ozkriff/zemeroth/issues/86>)
- Reduce text overlapping (<https://github.com/ozkriff/zemeroth/issues/214>)
- Multiple sprites per unit type (<https://github.com/ozkriff/zemeroth/issues/114>)
- Move back after a successful dodge (!!!) (<https://github.com/ozkriff/zemeroth/issues/117>)
- Items / artifacts (TODO: create an issue)
- Armor and Break stats (<https://github.com/ozkriff/zemeroth/issues/70>)
- Battle math (more interesting chances to hit enemies)
- Dynamic blood splatters (<https://github.com/ozkriff/zemeroth/issues/86>)
- Load/Save (TODO: find issue)
- Ranged units
- Write a guide (for beginners? this document?)
